console.log('start...');
// 入口文件

import Vue from 'vue';
// 导入路由模块
import VueRouter from 'vue-router'
// 手动安装路由
Vue.use(VueRouter);
// 加载路由js
import router from './router.js';

// 安装vue-resource
import VueResource from 'vue-resource';

Vue.use(VueResource);
Vue.http.options.emulateJSON = true;  //全局设置post数据提交格式

// 按需导入mint-ui 组件
// import { Header, Swipe, SwipeItem,Button,Lazyload  } from 'mint-ui';
// Vue.component(Header.name, Header);
// // banner 区域滑块
// Vue.component(Swipe.name, Swipe);
// Vue.component(SwipeItem.name, SwipeItem);
// Vue.component(Swipe.name, Swipe);
// Vue.component(Button.name, Button);
// Vue.use(Lazyload);
import MintUI from 'mint-ui'
import 'mint-ui/lib/style.css'
Vue.use(MintUI)

// 导入MUI 样式
import './lib/mui/css/mui.min.css';
import './lib/mui//css/icons-extra.css';
// 导入app 根组件
import app from './App.vue';

var vm = new Vue({
	el:'#app',
	render:function(c){
		return c(app);
	},
	// 装载路由
	router:router
});