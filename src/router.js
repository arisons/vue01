import VueRouter from 'vue-router'

// 导入4个tabbar 独立组件

import home from './components/tabbar/home.vue';
import member from './components/tabbar/member.vue';
import cart from './components/tabbar/cart.vue';
import search from './components/tabbar/search.vue';

import newslist from './components/newsLists/newsLists.vue';
import newsinfo from './components/newsLists/newsinfo.vue';

import contentImg from './components/content/contentImg.vue';

import mobileList from './components/mobile/mobileList.vue';
import mobileInfo from './components/mobile/mobileInfo.vue';

// 3. 创建路由对象
var router = new VueRouter({
  routes: [
    {path:'/', redirect:'/home'},
    {path:'/home',component:home},
    {path:'/member',component:member},
    {path:'/cart',component:cart},
    {path:'/search',component:search},
    {path:'/home/newslist',component:newslist},
    {path:'/home/contentImg',component:contentImg},
    {path:'/home/newslist/newsinfo/:id',component:newsinfo},
    {path:'/home/mobileList',component:mobileList},
    {path:'/home/mobileList/mobileInfo/:id',component:mobileInfo}
  ],
  linkActiveClass:'mui-active'  //覆盖默认的高亮显示样式
})

// 把路由对象暴露出去
export default router