## 这是一个由mint-ui 组件 + mui 框架组合成的项目

### 由牛逼的程序员（Arison）写的优雅代码，仅供参考。拒绝商用

### 目前暂时做到这几个页面吧，界面虽然，将就点

#### 页面用到的api 接口有：

> 新闻接口： [https://www.apiopen.top/meituApi](https://www.apiopen.top/meituApi)
> 美图接口：[https://www.apiopen.top/meituApi?page=1](https://www.apiopen.top/meituApi?page=1)
> 电影详情接口：[https://api.douban.com/v2/movie/subject/+电影名id](https://api.douban.com/v2/movie/subject/26942674)
> 电影列表接口：[https://api.douban.com/v2/movie/in_theaters](https://api.douban.com/v2/movie/in_theaters)
>
> 更多接口访问网址：
> + [https://blog.csdn.net/c__chao/article/details/78573737](https://blog.csdn.net/c__chao/article/details/78573737)
> +   [https://blog.csdn.net/mario_faker/article/details/79618235](https://blog.csdn.net/mario_faker/article/details/79618235)

页面展示的效果图如下：
![图一](giteeImg/vue1.png) ![图二](giteeImg/vue2.png)  ![图三](giteeImg/vue3.png) ![图四](giteeImg/vue4.png) ![图五](giteeImg/vue5.png)  





> 提交到码云步骤：
>- 1 `git add` .
>- 2` git commit -m '提交的信息'`
>- 3 `git push` 
